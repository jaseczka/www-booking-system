from django.contrib import admin

# Register your models here.
from confrooms.models import Vacancy, Room, Booking


class RoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'capacity', 'description', )
    search_fields = ('name', 'capacity', 'description', )


class VacancyAdmin(admin.ModelAdmin):
    list_display = ('room', 'day', 'start_time', 'end_time', )
    search_fields = ('room__name', 'day')


class BookingAdmin(admin.ModelAdmin):
    list_display = ('room', 'day', 'start_time', 'end_time', 'user', )
    search_fields = ('room__name', 'day', 'user__username', )
    readonly_fields = ('room', 'day', 'start_time', 'end_time', 'user', )

    def has_add_permission(self, request, obj=None):
        return False


admin.site.register(Room, RoomAdmin)
admin.site.register(Vacancy, VacancyAdmin)
admin.site.register(Booking, BookingAdmin)

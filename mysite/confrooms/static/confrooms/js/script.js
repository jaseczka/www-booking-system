/**
 * Created by mtepe_000 on 4/4/14.
 */
$(function() {
    $("tr").click(function() {
        if ($(this).find("a").attr("href") !== undefined)
            document.location = $(this).find("a").attr("href");
    });

    $("#room-vacancies-list").find("li").click(function() {
        $("#booking-day").attr("value", $(this).find(".day-field").text());
        $("#booking-start-time").attr("value", $(this).find(".start-time-field").text());
        $("#booking-end-time").attr("value", $(this).find(".end-time-field").text());
    });
});

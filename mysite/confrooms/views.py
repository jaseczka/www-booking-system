# -*- coding: utf-8 -*-
import datetime
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.views import generic
from django.shortcuts import render, get_object_or_404, redirect
from confrooms.models import Vacancy, Room, Booking


# Create your views here.
def login(request):
    username = request.POST['username']
    password = request.POST['password']
    next_page = request.POST['next']
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return redirect(next_page)

    else:
        return render(request, 'confrooms/index.html', {
            "login_message": "Nieprawidlowe haslo lub login",
        })


def logout(request):
    auth.logout(request)
    return redirect('confrooms:index')


class IndexView(generic.ListView):
    model = Room
    template_name = 'confrooms/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        next_page = self.request.REQUEST.get('next')
        if not next_page:
            next_page = reverse('confrooms:index')
        context['next'] = next_page
        return context


class BookView(generic.DetailView):
    model = Room
    template_name = 'confrooms/book.html'

    def get_context_data(self, **kwargs):
        context = super(BookView, self).get_context_data(**kwargs)
        context['now'] = datetime.datetime.now()
        return context


@login_required(login_url='confrooms:index')
def confirm(request, pk):
    room = get_object_or_404(Room, pk=pk)
    day = request.POST['day']
    start_time = int(request.POST['start_time'])
    end_time = int(request.POST['end_time'])

    if day and start_time and end_time:
        if start_time >= end_time:
            message = "Należy podać późniejszą godzinę zakończenia niż godzina rozpoczęcia."
        else:
            try:
                book_id = room.book(user=request.user, day=day, start_time=start_time,
                             end_time=end_time)
                if book_id:
                    return redirect('confrooms:booking_detail', book_id)
                else:
                    message = "Brak wolnego terminu, wybierz inny przedział czasu."

            except IntegrityError:
                message = "Rezerwacja nie powiodła się. Sprawdź ponownie dostępne terminy."
    else:
        message = "Należy uzupełnić wszystkie pola."

    return render(request, 'confrooms/book.html', {
        'room': room,
        'message': message,
    })


class ConfirmationView(generic.DetailView):
    model = Booking
    template_name = 'confrooms/booking_detail.html'

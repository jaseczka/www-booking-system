from django.conf.urls import patterns, url

from confrooms import views
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
   url(r'^$', views.IndexView.as_view(), name='index'),
   url(r'^book/(?P<pk>\d+)/$', login_required(views.BookView.as_view(),
                                              login_url='confrooms:index'),
       name='book'),
   url(r'^confirm/(?P<pk>\d+)/$', views.confirm, name='confirm'),
   url(r'^booking/(?P<pk>\d+)/$', login_required(views.ConfirmationView.as_view(),
                                                 login_url='confrooms:index'),
       name='booking_detail'),
   url(r'^login/$', views.login, name='login'),
   url(r'^logout/$', views.logout, name='logout'),

)
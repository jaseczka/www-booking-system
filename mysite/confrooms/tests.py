from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase
from confrooms.models import Room, Vacancy, Booking
import datetime


# Create your tests here.
from django.test.testcases import TransactionTestCase


class RoomMethodTest(TransactionTestCase):
    def setUp(self):
        u = User.objects.create_user(username='john')
        u.save()
        r = Room.objects.create(name="testowy", capacity=0)
        r.save()

    def test_current_vacancies_with_future_vacancy(self):
        now = datetime.datetime.now()
        r = Room.objects.get(capacity=0)
        vacancies_pre = r.vacancy_set.count()
        current_vacancies_pre = r.current_vacancies().count()
        v = Vacancy(room=r, day=now, start_time=0, end_time=now.hour+1)
        v.save()
        vacancies_post = r.vacancy_set.count()
        current_vacancies_post = r.current_vacancies().count()
        self.assertEqual(vacancies_pre+1, vacancies_post)
        self.assertEqual(current_vacancies_pre+1, current_vacancies_post)
        v1 = Vacancy(room=r, day=now + datetime.timedelta(days=1), start_time=1, end_time=2)
        v1.save()
        vacancies_post1 = r.vacancy_set.count()
        current_vacancies_post1 = r.current_vacancies().count()
        self.assertEqual(vacancies_post+1, vacancies_post1)
        self.assertEqual(current_vacancies_post+1, current_vacancies_post1)

    def test_current_vacancies_with_past_vacancy(self):
        now = datetime.datetime.now()
        r = Room.objects.get(capacity=0)
        vacancies_pre = r.vacancy_set.count()
        current_vacancies_pre = r.current_vacancies().count()
        v = Vacancy(room=r, day=now, start_time=0, end_time=now.hour)
        v.save()
        vacancies_post = r.vacancy_set.count()
        current_vacancies_post = r.current_vacancies().count()
        self.assertEqual(vacancies_pre+1, vacancies_post)
        self.assertEqual(current_vacancies_pre, current_vacancies_post)
        v1 = Vacancy(room=r, day=now - datetime.timedelta(days=1), start_time=0, end_time=1)
        v1.save()
        vacancies_post1 = r.vacancy_set.count()
        current_vacancies_post1 = r.current_vacancies().count()
        self.assertEqual(vacancies_post+1, vacancies_post1)
        self.assertEqual(current_vacancies_post, current_vacancies_post1)

    def test_book_whole_vacancy(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        v = Vacancy(room=r, day=now, start_time=st, end_time=et)
        v.save()
        vacancies_pre = r.vacancy_set.count()
        bookings_pre = r.booking_set.count()
        user = User.objects.get(username='john')
        self.assertNotEqual(r.book(user=user, day=now, start_time=st, end_time=et),
                         False)
        vacancies_post = r.vacancy_set.count()
        bookings_post = r.booking_set.count()
        self.assertEqual(vacancies_pre-1, vacancies_post)
        self.assertEqual(bookings_pre+1, bookings_post)


    def test_book_vacancy_partially1(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        v = Vacancy(room=r, day=now, start_time=st, end_time=et)
        v.save()
        vacancies_pre = r.vacancy_set.count()
        bookings_pre = r.booking_set.count()
        user = User.objects.get(username='john')

        self.assertNotEqual(r.book(user=user, day=now, start_time=st, end_time=et-1),
                         False)

        vacancies_post = r.vacancy_set.count()
        bookings_post = r.booking_set.count()

        self.assertEqual(vacancies_pre, vacancies_post)
        self.assertEqual(bookings_pre+1, bookings_post)

    def test_book_vacancy_partially2(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        v = Vacancy(room=r, day=now, start_time=st, end_time=et)
        v.save()
        vacancies_pre = r.vacancy_set.count()
        bookings_pre = r.booking_set.count()
        user = User.objects.get(username='john')
        self.assertNotEqual(r.book(user=user, day=now, start_time=st+1, end_time=et),
                         False)
        vacancies_post = r.vacancy_set.count()
        bookings_post = r.booking_set.count()
        self.assertEqual(vacancies_pre, vacancies_post)
        self.assertEqual(bookings_pre+1, bookings_post)

    def test_book_vacancy_partially3(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        v = Vacancy(room=r, day=now, start_time=st, end_time=et)
        v.save()
        vacancies_pre = r.vacancy_set.count()
        bookings_pre = r.booking_set.count()
        user = User.objects.get(username='john')
        self.assertNotEqual(r.book(user=user, day=now, start_time=st+1, end_time=et-1),
                            False)
        vacancies_post = r.vacancy_set.count()
        bookings_post = r.booking_set.count()
        self.assertEqual(vacancies_pre+1, vacancies_post)
        self.assertEqual(bookings_pre+1, bookings_post)

    def test_book_nonexistent_vacancy(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        user = User.objects.get(username='john')
        self.assertEqual(r.book(user=user, day=now, start_time=st, end_time=et),
                         False)
        self.assertEqual(r.vacancy_set.all().count(), 0)
        v = Vacancy(room=r, day=now, start_time=st, end_time=et-1)
        v.save()
        vacancies_pre = r.vacancy_set.count()
        bookings_pre = r.booking_set.count()
        user = User.objects.get(username='john')
        self.assertEqual(r.book(user=user, day=now, start_time=st, end_time=et),
                         False)
        vacancies_post = r.vacancy_set.count()
        bookings_post = r.booking_set.count()
        self.assertEqual(vacancies_pre, vacancies_post)
        self.assertEqual(bookings_pre, bookings_post)


class BookingMethodTest(TestCase):
    def setUp(self):
        u = User.objects.create_user(username='john')
        u.save()
        r = Room.objects.create(name="testowy", capacity=0)
        r.save()

    def test_overlapping_booking(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        u = User.objects.get(username='john')
        b1 = Booking(room=r, user=u, day=now, start_time=st, end_time=et)
        b1.save()
        b2 = Booking(room=r, user=u, day=now, start_time=st-1, end_time=et-1)
        self.assertRaises(ValidationError, b2.clean)
        d = now + datetime.timedelta(days=1)
        v = Vacancy(room=r, day=d, start_time=st, end_time=et)
        v.save()
        b3 = Booking(room=r, user=u, day=d, start_time=st+1, end_time=et+1)
        self.assertRaises(ValidationError, b3.clean)


class VacancyMethodTest(TestCase):
    def setUp(self):
        u = User.objects.create_user(username='john')
        u.save()
        r = Room.objects.create(name="testowy", capacity=0)
        r.save()

    def test_overlapping_booking(self):
        now = datetime.datetime.now()
        st = 10
        et = 13
        r = Room.objects.get(capacity=0)
        u = User.objects.get(username='john')
        v1 = Vacancy(room=r, day=now, start_time=st, end_time=et)
        v1.save()
        v2 = Vacancy(room=r, day=now, start_time=st+1, end_time=et-1)
        self.assertRaises(ValidationError, v2.clean)
        d = now + datetime.timedelta(days=1)
        b = Booking(room=r, user=u, day=d, start_time=st, end_time=et)
        b.save()
        v3 = Vacancy(room=r, day=d, start_time=st, end_time=et)
        self.assertRaises(ValidationError, v3.clean)
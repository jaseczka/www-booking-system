import datetime
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models, transaction


# Create your models here.
class Room(models.Model):
    name = models.CharField(max_length=50)
    capacity = models.PositiveIntegerField()
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    def current_vacancies(self):
        now = datetime.datetime.now()
        return self.vacancy_set.filter(day__gte=now). \
            exclude(day=now, end_time__lte=now.hour)

    def book(self, user, day, start_time, end_time):

        if start_time >= end_time:
            return False

        vacancies = self.vacancy_set.filter(day=day, start_time__lte=start_time,
                                            end_time__gte=end_time)
        if vacancies:
            v = vacancies[0]
        else:
            return False

        with transaction.atomic():
            v.delete()
            b = Booking(room=self, user=user, day=day, start_time=start_time,
                        end_time=end_time)
            b.save()
            if v.start_time != b.start_time:
                v1 = Vacancy(room=self, day=day, start_time=v.start_time,
                             end_time=start_time)
                v1.save()
            if v.end_time != b.end_time:
                v2 = Vacancy(room=self, day=day, start_time=end_time,
                             end_time=v.end_time)
                v2.save()

        return b.pk


def validate_timespan(room, day, start_time, end_time):
    if end_time <= start_time:
            raise ValidationError('End time value cannot be lower than the start time value.')
    vacancy_set = Vacancy.objects.filter(room=room, day=day)
    vacancy_set = vacancy_set.exclude(start_time__gte=end_time).exclude(end_time__lte=start_time)
    if vacancy_set.count() > 0:
        raise ValidationError('Time span cannot overlap with any existing vacancy time span.')
    booking_set = Booking.objects.filter(room=room, day=day)
    booking_set = booking_set.exclude(start_time__gte=end_time).exclude(end_time__lte=start_time)
    if booking_set.count() > 0:
        raise ValidationError('Time span cannot overlap with any existing booking time span.')


class Vacancy(models.Model):
    room = models.ForeignKey(Room)
    day = models.DateField()
    start_time = models.IntegerField(choices=[(x, x) for x in range(0, 24)])
    end_time = models.IntegerField(choices=[(x, x) for x in range(1, 25)])

    def __unicode__(self):
        return self.room.name + " " + unicode(self.day) + ", between " + \
               unicode(self.start_time) + " - " + unicode(self.end_time)

    def clean(self):
        validate_timespan(self.room, self.day, self.start_time, self.end_time)


class Booking(models.Model):
    room = models.ForeignKey(Room)
    user = models.ForeignKey(User)
    day = models.DateField()
    start_time = models.IntegerField(choices=[(x, x) for x in range(0, 24)])
    end_time = models.IntegerField(choices=[(x, x) for x in range(1, 25)])

    def __unicode__(self):
        return self.room.name + " " + unicode(self.day) + ", between " + \
               unicode(self.start_time) + " - " + unicode(self.end_time)

    def clean(self):
        validate_timespan(self.room, self.day, self.start_time, self.end_time)
